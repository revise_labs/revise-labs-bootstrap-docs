/**
 * Created by ksheppard on 22/04/2016.
 */
var gulp = require("gulp"),
    nunjucks = require("gulp-nunjucks-render"),
    less = require("gulp-less"),
    serve = require("gulp-serve"),
    fsExtra = require("fs-extra");

gulp.task('nunjucks', function() {
    return gulp.src(['docs/[^_]*.njk'])
        .pipe(nunjucks({
            path: 'docs'
        }))
        .pipe(gulp.dest('docs'));
});

gulp.task('less', function() {
    return gulp.src('docs/stylesheets/**/[^_]*.less')
        .pipe(less())
        .pipe(gulp.dest('docs/stylesheets'))
});

//gulp.task('serve', serve({root: '.', port: 80}));

gulp.task('docs', ['nunjucks', 'less'], serve({root: '.', port: 80}));

gulp.task('dist', function() {
   gulp.src('docs/stylesheets/bootstrap/*.css')
       .pipe(gulp.dest('stylesheets/components'));

    gulp.src('docs/stylesheets/fonts/*')
        .pipe(gulp.dest('stylesheets/fonts'));

    gulp.src('docs/stylesheets/icons/*')
        .pipe(gulp.dest('stylesheets/icons'));

    gulp.src('docs/javascript/bootstrap/*.js')
        .pipe(gulp.dest('javascript/components'));
});
