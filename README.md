# Revise Labs Bootstrap

This project aims to provide a nice base template for you to begin building fully responsive web layouts.

Components include:

- App bars
- Responsive drawer layout (with slide nav and swipe to dismiss navigation drawer)
- Form controls
- Dialogs
- Fonts

This framework differs from other such as Bootstrap because the markup required to create amazing layouts is minimal
and the class names are short, intuitive and easy to remember. See for yourself in the docs.

### Usage

To include this framework into your current project, run

```
npm install --save https://bitbucket.org/revise_labs/revise-labs-bootstrap
```

Then reference the components in your HTML by using

```
<link rel="stylesheet" href="node_modules/revise_labs_bootstrap/stylesheets/[component.css]" />
```

or

```
<script type="text/javascript" src="node_modules/revise_labs_bootstrap/javascript/[component.css]" />
```

### Documentation

To generate the documentation, `cd` into the `node_modules/revise_labs_bootstrap` directory and run `gulp docs`
then visit to `http://localhost/docs` to start reading.

### License

This project is the property of Revise Labs Ltd. made free to use
with love :-)
