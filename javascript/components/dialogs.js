/**
 * Created by Kevin on 5/14/2016.
 */
(function($) {
    $.fn.extend({
        dialog: function() {
            var $dialog = $(this[0]);
            var $dialogContent = $dialog.children(".dialog-content");

            function onDialogClose() {
                $dialog.removeClass("visible");
                $dialogContent.unbind("transitionend", onDialogClose);
                $("body").removeClass("noscroll");
            }

            return {
                open: function() {
                    $dialog.addClass("visible")
                        .addClass("open");
                    $("body").addClass("noscroll");
                },
                close: function() {
                    $dialog.removeClass("open");
                    $dialogContent.on("transitionend", onDialogClose);
                }
            };
        }
    });
})(jQuery);
