/**
 * Created by ksheppard on 29/04/2016.
 */
//TODO: Convert this into a format that will allow custom selectors for app bar, $toggleButton and $navigationDrawer
$(function() {
    var $toggleButton = $(".app-bar .drawer-toggle"),
        $navigationDrawer = $(".navigation-drawer"),
        $drawerContent = $(".navigation-drawer .drawer-content"),
        $body = $("body"),
        touchStartX, touchCurrentX, touchingSideNav = false, translation;

    $toggleButton.on("click", showNavigation);
    $navigationDrawer.on("click", hideNavigation);
    $drawerContent.on("click", blockClicks);
    $body.on("touchstart", onTouchStart)
         .on("touchmove", onTouchMove)
         .on("touchend", onTouchEnd);

    function onTouchStart($evt) {
        if($navigationDrawer.hasClass("visible")) {
            translation = 0;
            touchStartX = $evt.originalEvent.touches[0].pageX;
            touchCurrentX = touchStartX;
            touchingSideNav = true;
        }
    }

    function onTouchMove(evt) {
        if(touchingSideNav) {
            touchCurrentX = evt.originalEvent.touches[0].pageX;
            translation = Math.min(0, touchCurrentX - touchStartX);
            $drawerContent.attr("style", "transform: translateX("+translation+"px)");
        }
    }

    function onTouchEnd() {
        if (touchingSideNav) {
            touchingSideNav = false;
            $drawerContent.removeAttr("style");
            if(translation != 0) {
                if (translation <= -60) {
                    hideNavigation();
                } else {
                    showNavigation();
                }
            }
        }
    }

    function onTransitionEnd() {
        $navigationDrawer.removeClass("animating")
            .unbind("transitionend", onTransitionEnd);
    }

    function blockClicks(e) {
        e.stopPropagation();
    }

    function showNavigation(e) {
        //In case the .nav-button element is an anchor tag
        if(e) {
            e.preventDefault();
            //e.stopPropagation();
        }
        //Add the .visible and .animating class to the navigation drawer to make it visible and use transitions
        $navigationDrawer
            .addClass("visible")
            .addClass("animating")
            .on("transitionend", onTransitionEnd);
        $body.addClass("noscroll");
    }

    function hideNavigation() {
        //Remove the .visible and .animating class from the navigation drawer
        $navigationDrawer.addClass("animating")
            .removeClass("visible")
            .on("transitionend", onTransitionEnd);
        $body.removeClass("noscroll");
    }
});
