(function($) {
    $.fn.swapSVG = function() {
      //var _selector = selector == undefined ? "script[type=*=xml]" : selector;
        return this.each(function(index, elm) {
            var $elm = $(elm);
            $.ajax({
                method: 'GET',
                url: $elm.attr("src"),
                dataType: 'xml',
                success: function(data) {
                    var $span = $("<span></span>");
                    $span.html(data.documentElement);
                    $elm.replaceWith($span);
                }
            });
        });
    };
})(jQuery);